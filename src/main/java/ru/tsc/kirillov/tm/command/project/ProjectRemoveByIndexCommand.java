package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Удаление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final Project project = getProjectService().findOneByIndex(getUserId(), NumberUtil.fixIndex(index));
        getProjectTaskService().removeProjectById(getUserId(), project.getId());
    }

}
