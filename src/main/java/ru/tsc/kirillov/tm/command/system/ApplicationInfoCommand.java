package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.util.NumberUtil;

public final class ApplicationInfoCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение информации о системе.";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "безлимитно" : NumberUtil.formatBytes(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("Количество процесоров (ядер): " + availableProcessors);
        System.out.println("Свободно памяти: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Максимум памяти: " + maxMemoryFormat);
        System.out.println("Всего доступно памяти для JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Используется памяти в JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
